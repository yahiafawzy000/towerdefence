﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    [SerializeField]
    float _damageAmount;
    Vector2 _base;
    Transform _target;
    float _maxMoveDistanceFromBase;
    // Start is called before the first frame update



    Vector2 _bulletDirection;
      
    public void SetTargeAndBase(Vector2 _base, Transform _target,float _maxMoveDistanceFromBase) {
        this._base = _base;
        this._target = _target;
        this._maxMoveDistanceFromBase=_maxMoveDistanceFromBase;
    }

    private void FixedUpdate()
    {
        if (!_startMove)
        {
            //return;
        }

        if (_target)
        {
            if (Vector2.Distance(transform.position, _target.position) < 0.1f)
            {
                IHelth helth = _target.GetComponent<IHelth>();
                helth.Damage(_damageAmount);
                Destroy(this.gameObject);
            }
            _bulletDirection = _target.position - transform.position;
        }
        else if(_bulletDirection==Vector2.zero) {
            Destroy(gameObject);
        }
        

            if (Vector2.Distance(transform.position, _base) > _maxMoveDistanceFromBase)
            {                 
                Destroy(this.gameObject);
            }
            else
            {
                transform.position += (Vector3)_bulletDirection.normalized * 10 *Time.fixedDeltaTime;
            }
        
    }

    bool _startMove = false;
    private void OnDestroy()
    {
        _startMove = true;
    }

    internal void startMove()
    {
        _startMove = true;    
    }
}
