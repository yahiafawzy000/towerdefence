﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path : MonoBehaviour
{
    [SerializeField]
    ColorName colorName;
    enum ColorName { blue,green,red};

    public GameObject[] _points;


    private void OnDrawGizmos()
    {
        Color color=Color.black;
        switch (colorName) {
            case ColorName.green:
                color = Color.green;
                break;
            case ColorName.red:
                color = Color.red;
                break;
            case ColorName.blue:
                color = Color.blue;
                break;
        }
        for (int i = _points.Length-1; i > 0; i--) {
            Debug.DrawLine(_points[i].transform.position, _points[i - 1].transform.position,color);
        }
    }
}
