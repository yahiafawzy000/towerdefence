﻿using System;
using UnityEngine;
public class Rang : MonoBehaviour
{    
    GameObject _active;
    private float _raduis;
    public Action<Transform> _TargetChanged = delegate { };

    public float Raduis { get => _raduis;}

    private void Awake()
    {
        _raduis = GetComponent<CircleCollider2D>().radius;
        GetComponent<SpriteRenderer>().enabled = true;
        _TargetChanged += TarrgetCkhane;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (_active == collision.gameObject)
        {
            _active = null;
        }
        SetActiveTarget();
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        SetActiveTarget();
    }

    private void SetActiveTarget() {
        if (_active == null)
        {
            Collider2D coll = Physics2D.OverlapCircle(transform.position, _raduis, Physics2D.GetLayerCollisionMask(gameObject.layer));
            if (coll != null)
            {
                _active = coll.gameObject;
                _TargetChanged(_active.transform);
            }
            else
            {
                _TargetChanged(null);
            }
        }
    }

    void TarrgetCkhane(Transform t) {
        Debug.Log(t);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position,_raduis);
    }


}
