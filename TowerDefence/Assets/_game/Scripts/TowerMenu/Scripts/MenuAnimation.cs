﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAnimation : MonoBehaviour
{
    [SerializeField]
    float _animationSpeedFactore = 1f;
    [SerializeField]
    float _menuSacleFactore=1;
    Coroutine animtion;

    void Start()
    {
       animtion = StartCoroutine(MenuAnimationCorutine());
    }

    IEnumerator MenuAnimationCorutine() {
        while (true) {
            if (transform.localScale.x <= 1f)
            {
                transform.localScale += new Vector3(0.1f*_menuSacleFactore, 0.1f*_menuSacleFactore,0); 
            }
            else {
                transform.localScale = new Vector2(1.25f, 1.25f);
                StopCoroutine(animtion);
            }
            yield return new WaitForSeconds(0.01f* _animationSpeedFactore);
        }
    }

}
