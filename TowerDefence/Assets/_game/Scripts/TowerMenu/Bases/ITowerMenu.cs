﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEditor;
using TMPro;

[DefaultExecutionOrder(-100)]
public class ITowerMenu : MonoBehaviour
{
    [SerializeField]
    protected Button[] _Updatebuttons;
    protected ITower tower;
    protected int _curremntLevelMoney;
    protected int[] _upateValues=new int[3];

    protected virtual void Start()
    {
        tower = transform.parent.GetComponent<ITower>();
        _curremntLevelMoney = MoneyMangere.levelMangere._currentLevelMoney;
        SetUpMenuButton();       
    }

    public virtual void UpdateTower() {
        ITowerUpdater.UpdateTower(0);
        MoneyMangere.levelMangere.RemoveDromMoney(_upateValues[0]);
    }

    public virtual void RemoveTower() {
        ITowerUpdater.removeTower(0);
    } 

    void SetUpMenuButton() {
        for (int i = 0; i < _Updatebuttons.Length; i++)
        {
           _upateValues[i] = tower.towerData.NextLeveLtowersAndPrices[i]._NextLeveltowrPrice;
            Debug.Log(_upateValues[0]+"-------------->");
            if (_upateValues[i] > _curremntLevelMoney)
            {
                _Updatebuttons[i].interactable = false;
                _Updatebuttons[i].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = _upateValues[i]+"$"; 
            }
            else
            {
                _Updatebuttons[i].interactable = true;
                _Updatebuttons[i].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text =
                    _upateValues[i] + "$";
            }
        }
    }

    private void OnDisable()
    {
        
    }
}
