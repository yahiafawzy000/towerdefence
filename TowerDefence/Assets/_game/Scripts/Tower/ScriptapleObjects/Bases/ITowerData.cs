﻿
using UnityEngine;

public abstract class ITowerData : ScriptableObject
{
    [SerializeField]
    [Range(0.5f,10)]
    float fireSpeed;
    [SerializeField]
    Type type;

   
    [SerializeField]
    GameObject towerMenuPrefab;
    /// <summary>
    ///refrence to next Towers for update and base for rest
    /// </summary>
    [SerializeField]
    TowerAndPrice[] _nextLevelTowersAndPrices, _basePrefab;

    //add abullet prefab here;
    public float FireSpeed { get => fireSpeed;  }
    public Type Type { get => type;  }
    public GameObject TowerMenuPrefab { get => towerMenuPrefab;}


    public TowerAndPrice[] NextLeveLtowersAndPrices { get => _nextLevelTowersAndPrices; }
    public TowerAndPrice[] BasePrefab { get => _basePrefab; }
}

[System.Serializable]
public class TowerAndPrice
{
    public GameObject _NextLevelTowerPrefab;
    public int _NextLeveltowrPrice;
}





