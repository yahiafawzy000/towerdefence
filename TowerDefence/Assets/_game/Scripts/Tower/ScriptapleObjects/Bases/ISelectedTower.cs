﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ISelectedTower : ScriptableObject
{
    ITower _selectedTower;
    public ITower SelectedTower { get => _selectedTower; set => _selectedTower = value; }
}
