﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
/// <summary>
/// 
/// </summary>
public abstract class ITower : MonoBehaviour
{
    #region serilizedData
    [SerializeField]
    public ITowerData towerData;
    #endregion

    #region Data
    [HideInInspector]
    public static ITower _selectedTower;
    private static ITowerMenu _towerMenu;
    public Action TowerClicked = delegate { };
    #endregion


    protected virtual void Start()
    {
        TowerClicked += TowerClickedListener;
    }

    protected virtual void OnMouseDown()
    {
        TowerClicked();   
    }

    protected virtual void TowerClickedListener() {
        if (_selectedTower != null) {
            if (_selectedTower == this)
            {
                DeActiveTower();
            }
            else {
                activeTower();
            }
        }
        else {
            activeTower();
        }
    }

    protected virtual void activeTower() {
        if (Time.timeScale == 0) return;
        //_selectedTower.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
        _selectedTower = this;
        DestroyTowerMenu();
        _towerMenu = Instantiate(towerData.TowerMenuPrefab,transform.position,Quaternion.identity).GetComponent<ITowerMenu>();
        _towerMenu.transform.parent = this.transform;
        //_selectedTower.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
    }

    protected virtual void DeActiveTower()
    {
        if (Time.timeScale == 0) return;
        Debug.Log("de active tower");
        _selectedTower = null;
        DestroyTowerMenu();
    }

    protected void DestroyTowerMenu() {
        if (_towerMenu != null)
        {
            Destroy(_towerMenu.gameObject);
        }
    }

}

