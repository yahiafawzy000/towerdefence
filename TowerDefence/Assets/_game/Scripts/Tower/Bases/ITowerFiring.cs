﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ITowerFiring : MonoBehaviour
{
    #region Dats
    Rang rang;
    Transform _target;
    [SerializeField]
    Bullet bullet;
    #endregion



    private void Awake()
    {
        rang = transform.GetChild(0).GetComponent<Rang>();
        rang._TargetChanged += SetTargetListener;
    }

    void SetTargetListener(Transform target) {
        _target = target;
    }

    Coroutine Fire;

    private void FixedUpdate()
    {
        if (_target)
        {
            // Time.timeScale = 0;
            // bullet = Instantiate(bullet, transform.position, Quaternion.identity);
            //bullet.SetTargeAndBase(transform.position, _target, rang.Raduis);
            if(Fire==null)
            Fire = StartCoroutine(Fireing());

        }
        else {
            if (Fire != null)
            {
                StopCoroutine(Fire);
                Fire = null;
            }
        }
    }

    float _fireSpeed=0.1f;

    IEnumerator Fireing() {
        while (true) {
           Bullet bulletCopy = Instantiate(bullet, transform.position, Quaternion.identity);         
            bulletCopy.SetTargeAndBase(transform.position, _target, rang.Raduis);
            bulletCopy.startMove();
            yield return new WaitForSeconds(_fireSpeed);
        }
    }
}
