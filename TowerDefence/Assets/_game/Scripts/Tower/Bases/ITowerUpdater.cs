﻿using ModestTree;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ITowerUpdater : MonoBehaviour
{
   
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void UpdateTower(int index)
    {
        GameObject tower = Instantiate(ITower._selectedTower.towerData.NextLeveLtowersAndPrices[index]._NextLevelTowerPrefab, ITower._selectedTower.transform.position, Quaternion.identity);
        Destroy(ITower._selectedTower.gameObject);
        ITower._selectedTower = null;
        //throw new NotImplementedException();
    }

    public static void removeTower(int index)
    {
         Instantiate(ITower._selectedTower.towerData.BasePrefab[index]._NextLevelTowerPrefab, ITower._selectedTower.transform.position, Quaternion.identity);
        Destroy(ITower._selectedTower.gameObject);
        ITower._selectedTower = null;
        //throw new NotImplementedException();
    }

    private void DeActiveRang(SpriteRenderer spriteRenderer) {
        spriteRenderer.enabled = false;
    }

}
