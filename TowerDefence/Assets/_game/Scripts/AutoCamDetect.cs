﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoCamDetect : MonoBehaviour
{
    [SerializeField]
    SpriteRenderer _spriteTofit;
   
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject == null)
            return;
        if (Screen.width/Screen.height>_spriteTofit.bounds.size.x/_spriteTofit.bounds.size.y)
        {
            Camera.main.orthographicSize = _spriteTofit.bounds.size.y / 2;
        }
        else
        {
            Camera.main.orthographicSize = _spriteTofit.bounds.size.x * Screen.height / Screen.width * 0.5f;
        }
    }
}
