﻿using UnityEngine;
using System.Collections;
using ModestTree.Util;
using System;

public class MoneyMangere : MonoBehaviour
{

    [SerializeField]
    LevelData levelData;
    [HideInInspector]
    public int _currentLevelMoney;
    public Action<int> MoneyChanged = delegate { };

    public static MoneyMangere levelMangere;
    // Use this for initializa tion
    void Start()
    {
        levelMangere = this;
        _currentLevelMoney = levelData.LevelMoney;
    }

    public void AddToLevelMoney(int money) {
        _currentLevelMoney += money;
        MoneyChanged(money);
    }

    public void RemoveDromMoney(int money) {
        _currentLevelMoney -= money;
        MoneyChanged(money);
    }

}
