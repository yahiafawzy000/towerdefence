﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CammeraSize : MonoBehaviour
{
    enum WidthHehight { width, height };

    [SerializeField]
    SpriteRenderer _spriteTofit;
    [SerializeField]
    WidthHehight widthHehight;



    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (widthHehight == WidthHehight.height)
        {
            Camera.main.orthographicSize = _spriteTofit.bounds.size.y / 2;
        }
        else {
            Camera.main.orthographicSize = _spriteTofit.bounds.size.x * Screen.height / Screen.width * 0.5f;
        }   
    }
}