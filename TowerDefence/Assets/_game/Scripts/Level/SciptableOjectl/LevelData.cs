﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelData", menuName = "ScriptableObjects/LevelData", order =1)]
public class LevelData : ScriptableObject
{
    [SerializeField]
    private int levelMoney;
    [SerializeField]
    EnemyWave []enemyWaves;

    public int LevelMoney { get =>  levelMoney; }
    public EnemyWave[] EnemyWaves { get => enemyWaves; set => enemyWaves = value; }
}


[Serializable]
public class EnemyWave {
    [SerializeField]
    public EnemyData[] _wave;    
}

[Serializable]
public class EnemyData
{
    [SerializeField]
    public Enemey _enemy;
    [SerializeField]
    public int count;
}