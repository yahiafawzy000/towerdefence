﻿using System;
using UnityEngine;

public abstract class IHelth : MonoBehaviour
{
    #region SerilizedData
    [SerializeField]
    EneneyData eneneyData;    
    #endregion

    #region PrivteData
    float _currentHelth;
    public Action DeadEvent = delegate { };
    public Action<float> DamageEvent = delegate { };
    #endregion


    protected virtual void Start()
    {
        _currentHelth = eneneyData.MaxHelth;
        DeadEvent += Die;
    }

    public virtual void Damage(float damage) {
        _currentHelth -= damage;
        if (_currentHelth <= 0)
        {
            DeadEvent();
            Destroy(gameObject);
        }
        else {
            DamageEvent(_currentHelth/eneneyData.MaxHelth);
        }
    }   

    protected virtual void Die() {
        //Debug.Log("player die");       
    }

    protected virtual void Rest()    {
        Debug.Log("player rest");
    }

    protected virtual void OnDestroy()
    {
        Debug.Log("Destroy as Enemy");
        DeadEvent -= Die;
    }
}
