﻿using UnityEngine;
using UnityEngine.PlayerLoop;

public abstract class IEnemyMovment : MonoBehaviour
{

    #region SerilizedData
    [SerializeField]
    EneneyData eneneyData;
    #endregion


    #region privateData
    Vector2 _dir;
    float _speed;
    private int _position=0;
    private Path path;

    public Path Path { get => path; set => path = value; }
    #endregion


    private void Start()
    {
        _speed = eneneyData.Speed;  
    }

    protected virtual void FixedUpdate()
    {
        //get to last point
        if (Vector2.Distance(transform.position, path._points[path._points.Length - 1].transform.position) < 0.25f)
        {
            transform.position = path._points[0].transform.position;
            _position = 0;
            _dir = path._points[_position + 1].transform.position - transform.position;
        }
        else {
            if (Vector2.Distance(transform.position, path._points[_position + 1].transform.position) > 0.25f)
            {
                _dir = path._points[_position + 1].transform.position - transform.position;
            }
            else {
                _position++;
            }
        }

        transform.position += (Vector3)_dir.normalized*_speed*Time.fixedDeltaTime;
    }


}
