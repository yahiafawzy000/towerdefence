﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IHelthBar : MonoBehaviour
{
    #region SerilizedData
    [SerializeField]
    Transform pointer;
    #endregion

    #region Private
    bool _active;
    IHelth helth;
    #endregion

    // Start is called before the first frame update
    protected virtual void Start()
    {
        helth = GetComponent<IHelth>();
        helth.DamageEvent += ApllayDamageToHelthBar;
    }

    protected void ApllayDamageToHelthBar(float helth) {
        if (!_active)
        {
            pointer.transform.parent.gameObject.SetActive(true);
            _active = true;
        }
        pointer.transform.localScale = new Vector2(helth,
                                                    transform.localScale.y);
    }

    
}
