﻿using UnityEngine;

public abstract class IEnemey : MonoBehaviour
{
    IHelth helth;
    // Start is called before the first frame update
    protected virtual void Start()
    {
        helth = GetComponent<IHelth>();
        helth.DeadEvent += EnemeyDead;
    }

    // Update is called once per frame
    protected virtual void FixedUpdate()
    {
        //helth.Damage(0.1f);
    }

    protected virtual void EnemeyDead() {
       // Debug.Log("dead");        
    }

    protected virtual void OnDestroy()
    {
        Debug.Log("Destroy as Enemy");
        helth.DeadEvent -= EnemeyDead;
    }
}
