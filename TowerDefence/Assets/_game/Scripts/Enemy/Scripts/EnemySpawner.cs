﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    LevelData levelData;
    EnemyData []_wave;
    [SerializeField]
    Path path;

    int currentWave =0;
    private void Start()
    {

        // Disable screen dimming
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        _wave = levelData.EnemyWaves[0]._wave;
    }


    Coroutine stratWaves;
    public void StatWaves() {
        if(stratWaves==null)
        stratWaves = StartCoroutine(SpawnEnemy());
    }

    IEnumerator SpawnEnemy() {
        for (int i = 0; i < levelData.EnemyWaves.Length; i++) {
            _wave = levelData.EnemyWaves[i]._wave;
            Vector2 direction = path._points[0].transform.position - path._points[1].transform.position;
            int offest = 0;
            for (int j = 0; j < _wave.Length; j++) {
                for (int k = 0; k < _wave[j].count; k++)
                {
                    GameObject enemy = Instantiate(_wave[j]._enemy.gameObject, path._points[0].transform.position + (Vector3)direction.normalized * offest, Quaternion.identity);
                    enemy.GetComponent<IEnemyMovment>().Path = path;
                    offest++;
                }
            }
            
            yield return new WaitForSeconds(10);
        }
    }

}
