﻿using UnityEngine;
using System.Collections;

public abstract class IEnemeyData : ScriptableObject
{

    [SerializeField]
    [Range(1, 5)]
    float _speed;
    [SerializeField]
    [Range(1, 50)]
    float _maxHelth;
    [SerializeField]
    Type weakPoint;

    protected virtual void OnEnable()
    {
        Debug.Log("enabled");
    }

    public float Speed { get => _speed; }
    public float MaxHelth { get => _maxHelth; }
    public Type WeakPoint { get => weakPoint; }
}
